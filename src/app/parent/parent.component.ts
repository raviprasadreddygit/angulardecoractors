import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, AfterContentChecked, ViewChildren } from '@angular/core';
import { ChildComponent } from '../child/child.component';



@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit , AfterViewInit ,AfterContentChecked {
  
  
  
  
  
  Arraydata: any[];
  data1: any;
  OutputArrData: any[] =[]; 

  val :any = ' '
  myDate: any;
  constructor() { }



  @ViewChild('inputData') getInputData :ElementRef


  @ViewChild(ChildComponent) todayDate : ChildComponent

  @ViewChildren(ChildComponent) todayDate1 : ChildComponent


  viewChildData(){

    this.myDate =  this.todayDate1
    console.log("my Date is ::" , this.myDate ) 


//     this.val = this.getInputData.nativeElement
//    console.log(this.val.value);

  }


  ngAfterContentChecked(){
  //  setInterval(()=>{this.viewChildData }, 3000);



  }


  ngAfterViewInit(): void {


   
  
    setInterval(()=>{this.todayDate.today  = new Date}, 1000);
    
   
     this.myDate =  this.todayDate.today
  
    console.log("my Date is ::" , this.myDate ) 
  

    

    }
 


  ngOnInit() {
    this.getData();

  
    this.viewChildData()

  }



  myData(){
    return [{
        "userId": 1,
        "id": 1,
        "title": "delectus aut autem",
        "completed": false
      },
      {
        "userId": 1,
        "id": 2,
        "title": "quis ut nam facilis et officia qui",
        "completed": false
      },
      {
        "userId": 1,
        "id": 3,
        "title": "fugiat veniam minus",
        "completed": false
      },
      {
        "userId": 1,
        "id": 4,
        "title": "et porro tempora",
        "completed": true
      },
      {   
      "userId": 1,
      "id": 5,
      "title": "qui ullam ratione quibusdam voluptatem quia omnis",
      "completed": false      
      },
      {
        "userId": 1,
        "id": 6,
        "title": "qui ullam ratione quibusdam voluptatem quia omnis",
        "completed": false
      },
      {
        "userId": 1,
        "id": 7,
        "title": "illo expedita consequatur quia in",
        "completed": false
      },
      {
        "userId": 1,
        "id": 8,
        "title": "quo adipisci enim quam ut ab",
        "completed": true
      }]
  }



  getData(){
    this.Arraydata = this.myData()
    console.log(this.Arraydata)
  }



  getDataFromChildOutput(dataVal :any[]){

    this.OutputArrData = dataVal

    console.log(this.OutputArrData);
  }
}