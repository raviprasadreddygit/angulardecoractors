import { Component, OnInit, Input, Output ,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  selected_child_details: any;

  selected_child_detailsArr : any[] = []
  today: Date;


  constructor() { }

  ngOnInit() {

    this.getDateData()
  }


  @Input() childInputArr : any[]

  @Output() childOutputArr :EventEmitter<any> =  new EventEmitter<any>();



  getDateData(){
    this.today  = new Date()
  }


  outData(){

   // this.childOutputArr.emit('Hello Iam @Out...')
  }


  selectedData(i :any){

    console.log(i)

    this.selected_child_details  = i
    console.log(" selected_child_details  :: " , this.selected_child_details);
    
  //  this.selected_child_detailsArr.push(this.selected_child_details)

    this.childOutputArr.emit(this.selected_child_details)

  }



  selectedOutputData(){
//    this.childOutputArr.emit(this.selected_child_detailsArr)
  }

}


